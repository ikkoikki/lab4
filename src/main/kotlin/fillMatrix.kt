package org.lab4

/**
 * class containing algorithms for building matrix
 */

class matrixBuilder {
    var size: Int = 0
    val RIGHT: Int = 1
    val DOWN: Int = 2
    val LEFT: Int = 3
    val UP: Int = 4
    var direction: Int = RIGHT
    var row: Int = 0
    var column: Int = 0
    var counter: Int = 1
    var filled = mutableListOf<Pair<Int, Int>> ()
    var a = Pair(1, 2)

    fun BuildSize(size: Int) {
        this.size = size
    }

    fun stepByDirection() {
        if (direction == RIGHT) {
            column++
            return
        }

        if (direction == DOWN) {
            row++
            return
        }

        if (direction == LEFT) {
            column--
            return
        }

        if (direction == UP) {
            row--
            return
        }
    }

    fun changeDirection() {
        if (direction == RIGHT) {
            if (column + 1 >= size || filled.contains(Pair(row, column + 1))) {
                direction = DOWN
            }
            return
        }

        if (direction == DOWN) {
            if (row + 1 >= size || filled.contains(Pair(row + 1, column))) {
                direction = LEFT
            }
            return
        }

        if (direction == LEFT) {
            if (column <= 0 || filled.contains(Pair(row, column - 1))) {
                direction = UP
            }
            return
        }

        if (direction == UP) {
            if (row <= 0 || filled.contains(Pair(row - 1, column))) {
                direction = RIGHT
            }
            return
        }
    }

    fun fillCell(matrix: Matrix) {
        matrix.set(row, column, counter)
        filled.add(Pair(row, column))
        changeDirection()
        stepByDirection()
        counter++
    }
}

/**
 * func filling matrix according to the rule
 * 1  2  3  4
 * 12 13 14 5
 * 11 16 15 6
 * 10 9  8  7
 */
fun fillMatrix(matrix: Matrix, size: Int) {
    if (size <= 0) {
        throw IllegalArgumentException("Dim shouldn't be lower or equal 0")
    }
    val builder = matrixBuilder()
    builder.BuildSize(size)
    IntRange(0, size * size - 1).forEach {
        builder.fillCell(matrix)
    }
}
