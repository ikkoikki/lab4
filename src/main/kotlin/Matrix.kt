package org.lab4

interface Matrix {
    fun set(row: Int, column: Int, value: Int)
}
