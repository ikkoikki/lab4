
package org.lab4

import org.junit.jupiter.api.Test
import org.mockito.Mockito
import org.mockito.Mockito.mock

class TestFillMatrix {
    @Test
    fun `size equals 0`() {
        val m = mock(Matrix::class.java)
        try {
            fillMatrix(m, 0)
        } catch (ex: IllegalArgumentException) {
            print(ex.message)
        }
    }

    @Test
    fun `size lower than 0`() {
        val m = mock(Matrix::class.java)
        try {
            fillMatrix(m, -1)
        } catch (ex: IllegalArgumentException) {
            print(ex.message)
        }
    }

    @Test
    fun `Matrix 1 x 1`() {
        val m = mock(Matrix::class.java)

        fillMatrix(m, 1)

        Mockito.verify(m).set(0, 0, 1)
    }

    @Test
    fun `Matrix 3 x 3`() {
        val m = mock(Matrix::class.java)

        fillMatrix(m, 3)
        //      1, 2, 3,
        //      8, 9, 4,
        //      7, 6, 5,
        Mockito.verify(m).set(0, 0, 1)
        Mockito.verify(m).set(0, 1, 2)
        Mockito.verify(m).set(0, 2, 3)
        Mockito.verify(m).set(1, 2, 4)
        Mockito.verify(m).set(2, 2, 5)
        Mockito.verify(m).set(2, 1, 6)
        Mockito.verify(m).set(2, 0, 7)
        Mockito.verify(m).set(1, 0, 8)
        Mockito.verify(m).set(1, 1, 9)
    }

    @Test
    fun `Matrix 4 x 4`() {
        val m = Mockito.mock(Matrix::class.java)

        fillMatrix(m, 4)

        Mockito.verify(m).set(0, 0, 1)
        Mockito.verify(m).set(0, 1, 2)
        Mockito.verify(m).set(0, 2, 3)
        Mockito.verify(m).set(0, 3, 4)
        Mockito.verify(m).set(1, 3, 5)
        Mockito.verify(m).set(2, 3, 6)
        Mockito.verify(m).set(3, 3, 7)
        Mockito.verify(m).set(3, 2, 8)
        Mockito.verify(m).set(3, 1, 9)
        Mockito.verify(m).set(3, 0, 10)
        Mockito.verify(m).set(2, 0, 11)
        Mockito.verify(m).set(1, 0, 12)
        Mockito.verify(m).set(1, 1, 13)
        Mockito.verify(m).set(1, 2, 14)
        Mockito.verify(m).set(2, 2, 15)
        Mockito.verify(m).set(2, 1, 16)
    }
}
